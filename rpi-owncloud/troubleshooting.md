## Server does not start anymore

From time to time (most probably when I was forced to reboot the raspberry pi) suddently the owncloud-server is unreachable. Depending on the version of owncloud this is expressed by an blue owncloud-error page or by a meaningless page stateing that there was an error.

### Where are the logs?
- the NGINX log-output gets redirected to `stderr` and `stdout` repectively (that's the reason why you won't find any sensible output in `/var/log/nginx/access.log`). 
So instead, have a look at `sudo docker logs owncloud`
- Owncloud-Logs will be written to `/srv/http/owncloud/data/owncloud.log` - __for errors look there__!! 
(_you've configured it like that - and you always forget about this_)

### Owncloud cannot connect to MySQL
- Strangely after restarting the container the `owncloud` user is _sometimes_ broken - MySQL claims, that the user does not exist anymore but still has privileges (of course on the owncloud schema - which still does exist).
Drop the user and restart the `owncloud-db` container again. After that you'll be able to create a new `owncloud` user and set the necessary privileges again.
